from database.connector import *
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

class Cliente():
    __tablename__ = 'clientes'
    codigo = Column(Integer, primary_key=True)
    direccion = Column(String(25))
    celular = Column(String(20))
    email = Column(String(20))
    cuenta_bancaria= Column(String(20))

    # Relacion con personas creada
    persona_id = Column(Integer, ForeignKey('personas.codigo'))
    persona = relationship('Persona')

class Persona():
    __tablename__ = "personas"
    codigo = Column(Integer, primary_key=True)
    nombre = Column(String(25))
    apellido = Column(String(25))

    # Creamos la relación con cliente
    clientes = relationship(Cliente, backref="clientes")

    def serializar(self):
        data = {"id": self.codigo, "nombre": self.nombre, "apellido": self.apellido}
        return data





