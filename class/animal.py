from database.connector import *
from sqlalchemy import Column, Integer, String, ForeignKey, Float, Date
from sqlalchemy.orm import relationship

from model.veterinaria import Veterinaria


class Animal():
    __tablename__ = "animales"
    codigo = Column(Integer, primary_key=True)
    clasificacion = Column(String(30))
    especies = Column(String(30))
    edad = Column(String(30))
    color= Column(String(30))
    peso = Column(Float)


# Mascota  hereda de Animal
class Mascota(Animal):
    __tablename__ = "mascotas"
    codigo = Column(Integer, primary_key=True)
    alias = Column(String(30))
    especies = Column(String(30))
    raza = Column(String(30))
    color= Column(String(30))
    fechaNacimiento = Column(Date())
    peso = Column(Float)
    historial = Column(String(40))
    calendario = Column(Date())



    # Relacion creada

    veterinaria = relationship(Veterinaria, backref="veterinarias")
    animal = relationship(Animal, backref="animales")









