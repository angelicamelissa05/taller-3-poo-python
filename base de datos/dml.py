from database.connector import *
from model.cliente import Cliente
from model.animal import Animal
from model.animal import Mascota
from model.veterinaria import Veterinaria
from model.cliente import Persona


class DML():
    def getPersona(self):
        """ Consulta de Persona """
        query = session.query(Persona)

        data = query.all()
        informacion = []
        for info in data:
            temp = {}
            temp['codigo'] = info.codigo
            temp['nombre'] = info.nombre
            temp['apellido'] = info.apellido
            informacion.append(temp)
            print(temp)
        return informacion


    def addPersona(self, nom, ape):
        """ Agregar persona """
        new = Persona(nombre=nom, apellido=ape)
        session.add(new)
        session.commit()
        print("New persona ", new)
        return new.serializar()

    def delPersona(self, id):
        """ Metodo para eliminar persona """
        query = session.query(Persona).filter_by(codigo=id)
        print('SQL ===> ', query)
        temp = query.first()
        print('RESULTADO ==> ', temp)

        if temp is None:
            return 'El Usuario con el ID No Existente '

        session.delete(temp)
        session.commit()
        return True


    def updatePersona(self, id, nom, ape):
        """Metodo para actualizar información de persona"""
        query = session.query(Persona).filter_by(codigo=id)
        print(query)
        temp = query.first()
        print('Resultado ', temp)

        temp.nombre = nom
        temp.apellido = ape

        session.add(temp)
        session.commit()
        return True