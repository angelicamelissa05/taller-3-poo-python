from database.connector import *
from model.cliente import Cliente
from model.animal import Animal
from model.animal import Mascota
from model.veterinaria import Veterinaria
from model.cliente import Persona


class DML():
    """ Clase manejo de consulta de datos"""
    def getCliente(self):
        query = session.query(Cliente)
        print('======MOSTRAR DATOS DE CLIENTES======')
        
        for data in query.all():
            print(data.__dict__)
        print('\n============================\n')


    def getMascotas(self):
        query = session.query(Mascota)
        print('======MOSTRAR DATOS DEL ANIMAL=======')
        for data in query.all():
            print(data.__dict__)
        print('\n============================\n')


    def getVisitasVeterinaria(self):
        query = session.query(Veterinaria, Cliente, Mascota) \
            .join(Cliente, Cliente.codigo == Veterinaria.cliente_id) \
            .join(Mascota, Mascota.codigo == Veterinaria.mascota_id) \
            .all()
        print('_____IMPRESION DE HISTORIAL_____')
        for data in query:
            for item in data:
                print(item.__dict__)
            print()

        print('\n============================\n')

    def getUniversal(self, Modelo):
        query = session.query(Modelo)
        print('\n=====IMPRESION UNIVERSAL {0}======'.format(Modelo))
        for data in query.all():
            print(data.__dict__)
        print('\n=====FIN======\n')


